From Coq Require Import Reals Lists.List Lia Lra.

From Flocq Require Import Core BinarySingleNaN Operations.
From Gappa Require Import Gappa_tactic.
From Interval Require Import Lang_expr Lang_tac Tactic.

Open Scope R_scope.

Definition Rlb  :=            (-133) * Rpow2 (-16).
Definition Rubh := 4890627720347303  * Rpow2 (-61).

Definition c2 {Tl} : ArithExprTree Tl BinFloat := BinFl (-4503599627370493) (-53).
Definition c3 {Tl} : ArithExprTree Tl BinFloat := BinFl    750599937891995  (-51).
Definition c4 {Tl} : ArithExprTree Tl BinFloat := BinFl (-2251799816421115) (-53).
Definition c5 {Tl} : ArithExprTree Tl BinFloat := BinFl   1801448095886369  (-53).
Definition c6 {Tl} : ArithExprTree Tl BinFloat := BinFl (-6002655509745507) (-55).

Definition z2 {Tl} :
  ArithExprTree (BinFloat :: Tl) BinFloat :=
  Op MUL (Var 0) (Var 0).

Definition p56 {T1 Tl} :
  ArithExprTree (T1 :: BinFloat :: Tl) BinFloat :=
  Fma c6 (Var 1) c5.

Definition p34 {T1 T2 Tl} :
  ArithExprTree (T2 :: T1 :: BinFloat :: Tl) BinFloat :=
  Fma c4 (Var 2) c3.

Definition ph0 {Tl} :
  ArithExprTree (BinFloat :: BinFloat :: BinFloat :: Tl) BinFloat :=
  Fma (Var 1) (Var 2) (Var 0).

Definition ph1 {T1 T2 T3 Tl} :
  ArithExprTree (BinFloat :: T3 :: T2 :: T1 :: BinFloat :: Tl) BinFloat :=
  Fma (Var 0) (Var 4) c2.

Definition ph {T1 T2 T3 Tl} :
  ArithExprTree (BinFloat :: T3 :: T2 :: T1 :: BinFloat :: Tl) BinFloat :=
  Op MUL (Var 0) (Var 4).

Definition coreMathLn__ {Tl} : ArithExprTree (BinFloat :: Tl) BinFloat :=
Let z2 (Let p56 (Let p34 (Let ph0 (Let ph1 ph)))).


Import Rrnd.

Coercion B2R : binary_float >-> R.

Notation "[: v1 ; .. ; vn :]" := (v1, .. (vn, tt) .. ).

Lemma log_correct' : forall z,
  is_finite z = true -> Rlb <= z <= Rubh ->
  is_finite (evalFloat (@coreMathLn__ nil) [:z:] mode_NE) = true /\
  Rabs ((evalFloat (@coreMathLn__ nil) [:z:] mode_NE) - (ln (1 + z) - z)) <= 2485 * Rpow2 (-80).
Proof. intros z fin_z b_z. unfold coreMathLn__. remove_float.
extract_real z as z Hformatz. unfold Rlb, Rubh. intros Hboundz.

split.
{ now simplify_wb. }

set (Rph := evalExact (@coreMathLn__ nil) (z, tt)).
assert (H : Rabs (Rph - (ln (1 + z) - z)) <= 845 * Rpow2 (-80)).
{ cbn -[bpow]. interval with (i_taylor z, i_bisect z, i_prec 80). }
revert H. cbn -[bpow]. unfold fma, Rnd, round_mode, emin, emax, prec. gappa.
Qed.
