From Coq Require Import Reals Lists.List Lia Lra.

From Flocq Require Import Core BinarySingleNaN Operations.
From Gappa Require Import Gappa_tactic.
From Interval Require Import Lang_expr Lang_tac Tactic.

Open Scope R_scope.

Lemma rel_helper :
  forall xa xe b : R,
  xe <> 0 ->
  (Rabs ((xa - xe) / xe) <= b <-> Rabs (xa - xe) <= b * Rabs xe).
Proof.
  intros xa xe b Zx.
  unfold Rdiv.
  rewrite Rabs_mult, Rabs_inv by exact Zx.
  split ; intros H.
  - apply Rmult_le_reg_r with (/ Rabs xe).
    apply Rinv_0_lt_compat.
    now apply Rabs_pos_lt.
    rewrite Rmult_assoc, Rinv_r, Rmult_1_r.
    exact H.
    now apply Rabs_no_R0.
  - apply Rmult_le_reg_r with (Rabs xe).
    now apply Rabs_pos_lt.
    rewrite Rmult_assoc, Rinv_l, Rmult_1_r.
    exact H.
    now apply Rabs_no_R0.
Qed.


(** Constants **)

(* As Coq reals *)

Definition RLog2h   :=    3048493539143 * Rpow2 (-42).
Definition RLog2l   :=  544487923021427 * Rpow2 (-93).
Definition RInvLog2 := 3248660424278399 * Rpow2 (-51).

Definition Rp0      :=                1 * Rpow2  (-2).
Definition Rp1      := 4002712888408905 * Rpow2 (-59).
Definition Rp2      := 1218985200072455 * Rpow2 (-66).
Definition Rq0      :=                1 * Rpow2  (-1).
Definition Rq1      := 8006155947364787 * Rpow2 (-57).
Definition Rq2      := 4573527866750985 * Rpow2 (-63).

(* As Flocq binary floats *)

Definition FPLog2h   := binnorm mode_NE    3048493539143 (-42).
Definition FPLog2l   := binnorm mode_NE  544487923021427 (-93).
Definition FPInvLog2 := binnorm mode_NE 3248660424278399 (-51).

Definition FPp0      := binnorm mode_NE                1  (-2).
Definition FPp1      := binnorm mode_NE 4002712888408905 (-59).
Definition FPp2      := binnorm mode_NE 1218985200072455 (-66).
Definition FPq0      := binnorm mode_NE                1  (-1).
Definition FPq1      := binnorm mode_NE 8006155947364787 (-57).
Definition FPq2      := binnorm mode_NE 4573527866750985 (-63).

(* As trees *)

Definition Log2h   {Tl} : ArithExprTree Tl BinFloat := BinFl    3048493539143 (-42).
Definition Log2l   {Tl} : ArithExprTree Tl BinFloat := BinFl  544487923021427 (-93).
Definition InvLog2 {Tl} : ArithExprTree Tl BinFloat := BinFl 3248660424278399 (-51).

Definition p0 {Tl} : ArithExprTree Tl BinFloat := BinFl                1 ( -2).
Definition p1 {Tl} : ArithExprTree Tl BinFloat := BinFl 4002712888408905 (-59).
Definition p2 {Tl} : ArithExprTree Tl BinFloat := BinFl 1218985200072455 (-66).
Definition q0 {Tl} : ArithExprTree Tl BinFloat := BinFl                1 ( -1).
Definition q1 {Tl} : ArithExprTree Tl BinFloat := BinFl 8006155947364787 (-57).
Definition q2 {Tl} : ArithExprTree Tl BinFloat := BinFl 4573527866750985 (-63).


(** Main algorithm **)

(* Argument reduction *)
Definition k  {Tl} : ArithExprTree (BinFloat :: Tl) BinFloat :=
  NearbyInt (Op MUL (Var 0) InvLog2).
Definition t  {Tl} : ArithExprTree (BinFloat :: BinFloat :: Tl) BinFloat :=
(*Op SUB (Op SUB (Var 1) (OpExact MUL (Var 0) Log2h)) (Op MUL (Var 0) Log2l). *)
  Op SUB (OpExact SUB (Var 1) (OpExact MUL (Var 0) Log2h)) (Op MUL (Var 0) Log2l).

(* Approximation using a rational fraction *)
Definition t2 {Tl} : ArithExprTree (BinFloat :: Tl) BinFloat :=
  Op MUL (Var 0) (Var 0).
Definition p  {Tl} : ArithExprTree (BinFloat :: Tl) BinFloat :=
  Op ADD p0 (Op MUL (Var 0) (Op ADD p1 (Op MUL (Var 0) p2))).
Definition q  {Tl} : ArithExprTree (BinFloat :: BinFloat :: Tl) BinFloat :=
  Op ADD q0 (Op MUL (Var 1) (Op ADD q1 (Op MUL (Var 1) q2))).

Definition body_ : ArithExprTree (BinFloat :: BinFloat :: BinFloat :: nil) BinFloat :=
 (Let
    t2
 (Let
    p
 (Let
    q
 (Op ADD
   (Op DIV
     (Op MUL (Var 3) (Var 1))
     (Op SUB (Var 0) (Op MUL (Var 3) (Var 1))))
   (BinFl 1 (-1)))))).

Definition cw_exp_ : ArithExprTree (BinFloat :: nil) BinFloat :=
  Let k (Let t body_).

Definition FPcw_exp_ x := evalFloat cw_exp_ (x, tt) mode_NE.


Import Rrnd.

(** Correctness of argument reduction (first two steps in the algorithm) **)

Lemma argument_reduction :
  forall x : R,
  generic_format radix2 (FLT_exp (-1074) 53) x ->
  -746 <= x <= 710 ->
  let k  := @nearbyint mode_NE (x * RInvLog2)%rnd in
  let tf := ((x - Rmult k RLog2h)%rnd - (k * RLog2l)%rnd)%rnd in
  let te := x - k * ln 2 in
  (x - Rmult k RLog2h)%rnd = x - k * RLog2h /\
  Rabs tf <= 355 / 1024 /\
  Rabs (tf - te) <= 65537 * Rpow2 (-71).
Proof with auto with typeclass_instances.
intros x Fx Bx k tf te.
assert (Rabs x <= 5/16 \/ 5/16 <= Rabs x <= 746) as [Bx'|Bx'] by gappa.
- assert (k = 0).
    clear -Bx'.
    refine (let H := _ in Rle_antisym _ _ (proj2 H) (proj1 H)).
    unfold k, nearbyint, Rnd, round_mode, RInvLog2, RLog2h, Rnd; gappa.
  unfold tf, te, Rnd, round_mode.
  rewrite H.
  rewrite 3!Rmult_0_l.
  rewrite round_0...
  rewrite 2!Rminus_0_r.
  rewrite round_generic with (2 := Fx)...
  gappa.
- assert (Hl: - 1 * Rpow2 (-102) <= RLog2l - (ln 2 - RLog2h) <= 0).
    unfold RLog2l, RLog2h ;
    interval with (i_prec 110).
  assert (Ax: x = x * RInvLog2 * (1 / RInvLog2)).
    field.
    unfold RInvLog2 ; interval.
  unfold te.
  replace (x - k * ln 2) with (x - k * RLog2h - k * (ln 2 - RLog2h)) by ring.
  revert Hl Ax.
  unfold tf, te, k, nearbyint, Rnd, round_mode, RInvLog2, RLog2h, RLog2l; gappa.
Qed.


(** Correctness of approximation **)

Lemma method_error :
  forall t : R,
  let t2 := t * t in
  let p := Rp0 + t2 * (Rp1 + t2 * Rp2) in
  let q := Rq0 + t2 * (Rq1 + t2 * Rq2) in
  let f := 2 * ((t * p) / (q - t * p) + 1/2) in
  Rabs t <= 355 / 1024 ->
  Rabs ((f - exp t) / exp t) <= 23 * Rpow2 (-62).
Proof.
intros t t2 p q f Ht.
unfold f, q, p, t2, Rp0, Rp1, Rp2, Rq0, Rq1, Rq2.
interval with (i_bisect t, i_taylor t, i_degree 9, i_prec 70).
Qed.


(** Full correctness (without final scaling) **)

Coercion B2R : binary_float >-> R.

Lemma cw_exp__correct :
  forall x, is_finite x = true -> -746 <= x <= 710 ->
    is_finite (FPcw_exp_ x) = true /\
    let k := @nearbyint mode_NE (x * RInvLog2)%rnd in
    let x' := x - k * ln 2 in Rabs (2 * FPcw_exp_ x - exp x') <= Rpow2 (-51) * Rabs (exp x').
Proof. intros x fin_x b_x. unfold FPcw_exp_.


(* Get rid of floating point expressions in the goal *)
remove_float. extract_real x as x0 Hformat_x0. intros Hbx0.
(* split. { simplify_wb_taylor. } would prove all goals besides exact operations *)
set (k0 := @nearbyint mode_NE (x0 * RInvLog2)%rnd).


(* Prove correctness of argument reduction first *)

(* Go one step down the expression tree *)
unfold cw_exp_. change k0 with (evalRounded (@k nil) (x0, tt) mode_NE).
assert_on_let (fun x => x = k0) as x1 H1;
[clear k0; simpl M2R_list; now simplify_wb | easy |].

interval_intro (IZR (ZnearestE (round radix2 (FLT_exp emin prec) ZnearestE (x0 * RInvLog2)))) as Haux.
rewrite <-round_FIX_IZR in Haux. unfold k0, nearbyint in H1. clear k0.
unfold Rnd in H1. simpl round_mode in H1. rewrite <-H1 in Haux.
assert (H' : generic_format radix2 (FIX_exp 0) x1).
{ rewrite H1. apply generic_format_round. apply FIX_exp_valid. apply valid_rnd_N. }

(* Go one step further down *)
assert_on_let
 (fun x => let t := x0 - x1 * ln 2 in
  Rabs x <= 355 / 1024 /\
  Rabs (x - t) <= 65537 * Rpow2 (-71)) as x2 [H20 H21].
{ simpl M2R_list. simplify_wb. split.
  2: { rewrite H1. unfold Rnd, round_mode, emin, emax, prec, RInvLog2. gappa. }
  rewrite H1. now apply argument_reduction. }
{ intros _. rewrite H1. generalize (argument_reduction x0 Hformat_x0 Hbx0).
  intros [Heq H]. unfold nearbyint, Rnd, round_mode, RLog2l, RLog2h in Heq, H.
  cbn -[bpow emin]. rewrite <-Heq. apply H. }


(* Argument reduction is done, now everything can be handled by CoqInterval or Gappa *)

split; [simpl M2R; simpl M2R_list; now simplify_wb |].
clear -Haux H20 H21.
  cut (Rabs (2 * evalExact body_
    (x2, (x1, (x0, tt))) - exp x2) <= 23 * Rpow2 (-62) * Rabs (exp x2)).
  2 : { apply rel_helper. apply exp_neq_0. now apply method_error. }
  cut (Rabs (exp x2 - exp (x0 - x1 * ln 2)) <= 33 * Rpow2 (-60) * Rabs (exp (x0 - x1 * ln 2))).
  2 : { apply rel_helper. apply exp_neq_0. unfold Rdiv. rewrite Rmult_minus_distr_r.
    rewrite Rinv_r by apply Rgt_not_eq, exp_pos.
    rewrite <-exp_Ropp, <-exp_plus. revert H21.
    unfold Rminus; generalize (x2 + - (x0 + - (x1 * ln 2))); clear.
    intros r Hr; interval with (i_prec 60). }
  cbn -[bpow]. intros H H''.
  unfold nearbyint, Rnd, round_mode, maxval, emin, prec, emax, RInvLog2 in *.
  Time gappa. (* Takes about 5 seconds. *)
Qed.
