Test examples of the main project.

Requires Coq support for Gappa (https://gappa.gitlabpages.inria.fr) and CoqInterval (https://coqinterval.gitlabpages.inria.fr):

```
opam install coq-gappa
opam install coq-interval
```
